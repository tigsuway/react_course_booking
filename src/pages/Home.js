// Import react
import React from 'react';

// Acquire components previously built
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {
	return (
		<div>
			<Banner />
			<Highlights />
		</div>
	);
}
