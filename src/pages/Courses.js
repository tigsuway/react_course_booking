import React from 'react';

import Course from '../components/Course';

import coursesData from '../data/courses';

export default function Courses() {
	const courseContent = coursesData.map((course) => {
		return <Course key={course.id} course={course} />;
	});
	return <div>{courseContent}</div>;
}
