export default [
	{
		id: 'wdc001',
		name: 'PHP-Laravel',
		description: 'Laravel is it!',
		price: 990.0,
		onOffer: true,
	},
	{
		id: 'wdc002',
		name: 'Python - Django',
		description: 'Django is it!',
		price: 990.0,
		onOffer: true,
	},
	{
		id: 'wdc003',
		name: 'Java - Springboot',
		description: 'Springboot is it!',
		price: 990.0,
		onOffer: true,
	},
];
