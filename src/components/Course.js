import React from 'react';

import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';

import PropTypes from 'prop-types';

export default function Course({ course }) {
	const { name, description, price } = course;
	return (
		<Card>
			<Card.Body>
				<Card.Title> {name} </Card.Title>
				<Card.Text>
					<span className="subtitle">Description: </span>
					<br />
					{description}
					<br />
					<span className="subtitle">Price:</span>
					<br />
					{price}
					<br />
					<span className="subtitle">Enrollees:</span> <br />
					<br />
					<span className="subtitle">Seats:</span> <br />
					<br />
				</Card.Text>
				<Button variant="info">Enroll</Button>
			</Card.Body>
		</Card>
	);
}

//propTypes is a method in the ES6 class-style React properties
Course.propTypes = {
	// shape() is used to check that a property of an object conforms to a specific shape
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
	}),
};
