import React from 'react';

import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button';
// default exports

import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
// bootstrap grid system (named exports)

// lets create a function that will return/render our components
export default function Banner() {
	return (
		<Row>
			<Col>
				<Jumbotron
					id="hero-jumbo"
					className="jumbotron jumbotron-fluid"
				>
					<div className="container">
						<div className="row">
							<div className="col-md-12">
								<h1 className="display-4 text-white align-bottom">
									SOLSYM FITNESS
								</h1>
								<p className="lead text-white">
									Helping you find time to workout.
								</p>
								<Button
									id="profile-button"
									className="btn btn-primary btn-lg"
									role="button"
								>
									Schedule Now!
								</Button>
							</div>
						</div>
					</div>
				</Jumbotron>
			</Col>
		</Row>
	);
}
