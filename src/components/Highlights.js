import React from 'react';

import Card from 'react-bootstrap/Card';

import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';

export default function Highlights() {
	return (
		<Row>
			{/* First Card */}
			<Col xs={12} md={4}>
				<Card className="cardHighlights">
					<Card.Body>
						<Card.Title>Learn From Home</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing
							elit. Quis enim obcaecati, soluta nihil delectus
							itaque expedita, nemo repellendus perspiciatis a nam
							quidem minima natus, modi quod. Odio illum autem
							itaque.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			{/* Second Card */}
			<Col xs={12} md={4}>
				<Card className="cardHighlights">
					<Card.Body>
						<Card.Title> Study Now Pay Later </Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing
							elit. Quis enim obcaecati, soluta nihil delectus
							itaque expedita, nemo repellendus perspiciatis a nam
							quidem minima natus, modi quod. Odio illum autem
							itaque.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			{/* Third Card */}
			<Col xs={12} md={4}>
				<Card className="cardHighlights">
					<Card.Body>
						<Card.Title> Be Part Of Our Community </Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing
							elit. Quis enim obcaecati, soluta nihil delectus
							itaque expedita, nemo repellendus perspiciatis a nam
							quidem minima natus, modi quod. Odio illum autem
							itaque.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	);
}
