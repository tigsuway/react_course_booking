import React from 'react';

import Navbar from 'react-bootstrap/Navbar';
import Papaya from 'react-bootstrap/Nav';

// 2 kinds of exports
//=> export default -> these can be repackaged inside another variable but you have to retrieve them using exact path/location.
//=> named export => the are identified with "{} but they cannot be repackaged."

export default function Header() {
	return (
		<Navbar bg="light" expand="lg">
			<Navbar.Brand href="#home">
				React-React Course Booking 87
			</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Papaya className="mr-auto">
					<Papaya.Link href="#"> Home </Papaya.Link>
				</Papaya>
			</Navbar.Collapse>
		</Navbar>
	);
}
