import './App.css';

import Header from './components/NavBar';
// import Home from './pages/Home';
import Courses from './pages/Courses';
import { Container } from 'react-bootstrap';
// Solving the problem of multiple jsx elements inside return.
// Solution #1: Grouping elements using <React.Fragment>

export default function App() {
	return (
		<div>
			<Header />
			<Container>
				<Courses />
			</Container>
		</div>
	);
}
